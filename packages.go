package main

import (
	"fmt"
	"strings"
)

func main() {
    //contains-mavjudmi
	//Count-nechta
	//HasPrefix-oldida yoki boshida
	//HasSuffix-oxirida
	fmt.Println(strings.Contains("Yusufbek","bek"))
	fmt.Println(strings.Count("Yusufbek","u"))
	fmt.Println(strings.HasPrefix("Yusufbek","Yusuf"))
	fmt.Println(strings.HasSuffix("Yusufbek","Yusuf"))
	fmt.Println(strings.Index("Alloh","l"))
	
}